import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Book} from '../models/book.model';
import {BookService} from './book.service';

@Component({
  templateUrl: './edit-book.component.html'
})
export class EditBookComponent implements OnInit {

  book: Book = new Book();

  constructor(private router: Router, private route: ActivatedRoute,
              private bookService: BookService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        console.log('1');
        this.bookService.getBookById(id).subscribe((book: any) => {
          console.log('2');
          if (book) {
            this.book = book;
          } else {
            console.log(`Book with id '${id}' not found, returning to list`);
            this.gotoList();
          }
        });
      }
    });
  }

  editBook(): void {
    this.bookService.editBook(this.book)
      .subscribe(data => {
        alert('Book edited successfully.');
      });
    this.bookService.getBooks();
    this.router.navigate(['books']);
  }

  gotoList() {
    this.bookService.getBooks();
    this.router.navigate(['books']);
  }

}
