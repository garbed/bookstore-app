export class Book {
  id: string;
  name: string;
  author: string;
  pages: string;
  editorial: string;
}
